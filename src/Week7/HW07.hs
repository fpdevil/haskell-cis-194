{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE RecordWildCards     #-}
{-# OPTIONS_GHC -Wall #-}
module HW07 where

import           Cards
import           Prelude              hiding (mapM)

import           Control.Monad        hiding (liftM, mapM)
import           Control.Monad.Random
import           Data.Functor
import           Data.Monoid
import           Data.Vector          (Vector, cons, (!), (!?), (//))
import           System.Random

import qualified Data.Vector          as V


-- Exercise 1 -----------------------------------------

liftM :: Monad m => (a -> b) -> m a -> m b
liftM fn m = do
    x <- m
    return (fn x)

swapV :: Int -> Int -> Vector a -> Maybe (Vector a)
swapV x y vec = do
    justX <- vec V.!? y
    justY <- vec V.!? x
    return (vec V.// [(x, justX), (y, justY)])

-- Exercise 2 -----------------------------------------

mapM :: Monad m => (a -> m b) -> [a] -> m [b]
mapM m = seqA . fmap m
    where seqA [] = pure []
          seqA (x : xs) = (:) <$> x <*> seqA xs

getElts :: [Int] -> Vector a -> Maybe [a]
getElts xs v = mapM (v V.!?) xs

-- Exercise 3 -----------------------------------------

type Rnd a = Rand StdGen a

randomElt :: Vector a -> Rnd (Maybe a)
randomElt v = getRandomR (0, V.length v) >>= (\x -> return (v V.!? x))

-- Exercise 4 -----------------------------------------

randomVec :: Random a => Int -> Rnd (Vector a)
randomVec x = liftM V.fromList $ forM [0 .. x] (const getRandom)

randomVecR :: Random a => Int -> (a, a) -> Rnd (Vector a)
randomVecR x (lo, hi) = liftM V.fromList $ forM [0 .. x] (const $ getRandomR (lo, hi))

-- Exercise 5 -----------------------------------------

shuffle :: Vector a -> Rnd (Vector a)
shuffle v = auxShuffle v (V.length v - 1)
        where
        auxShuffle vec 0 = return vec
        auxShuffle vec i = do
                   j <- getRandomR (0, i)
                   case swapV i j vec of
                     Nothing   -> return V.empty
                     Just xvec -> auxShuffle xvec (i - 1)

-- Exercise 6 -----------------------------------------

partitionAt :: Ord a => Vector a -> Int -> (Vector a, a, Vector a)
partitionAt v i = (vlow, pivot, vhigh)
            where
            pivot  = v V.! i
            vlow   = V.filter (< pivot) v
            vhigh  = V.filter (>= pivot) v

-- Exercise 7 -----------------------------------------

-- Quicksort
quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = quicksort [ y | y <- xs, y < x ]
                   <> (x : quicksort [ y | y <- xs, y >= x ])

qsort :: Ord a => Vector a -> Vector a
qsort v
    | V.null v  = V.empty
    | otherwise = qsort [y | y <- xs, y < x]
               <> (x `cons` qsort [y | y <- xs, y >= x])
               where
               (x, xs) = (V.head v, V.tail v)

-- Exercise 8 -----------------------------------------

qsortR :: Ord a => Vector a -> Rnd (Vector a)
qsortR v
    | V.null v  = return V.empty
    | otherwise = do
      pivot <- getRandomR (0, V.length v - 1)
      let (vlow, vpivot, vhigh) = partitionAt v pivot
      vfirst <- qsortR vlow
      vlast  <- qsortR vhigh
      return (vfirst <> (vpivot `cons` vlast))

-- Exercise 9 -----------------------------------------

-- Selection
select :: Ord a => Int -> Vector a -> Rnd (Maybe a)
select 0 _ = return Nothing
select i v
    | V.null v  = return Nothing
    | otherwise = do
      randomPivot <- getRandomR (0, V.length v - 1)
      let (left, pivot, right) = partitionAt v randomPivot
      case i `compare` V.length left of
        LT -> select i left
        EQ -> return (Just pivot)
        GT -> select (i - V.length left - 1) right



-- Exercise 10 ----------------------------------------

allCards :: Deck
allCards = V.fromList $ pure Card <*> [Two .. ] <*> [Spade, Heart, Club, Diamond]

newDeck :: Rnd Deck
newDeck =  shuffle allCards

-- Exercise 11 ----------------------------------------

nextCard :: Deck -> Maybe (Card, Deck)
nextCard d
    | V.null d  = Nothing
    | otherwise = Just (V.head d, V.tail d)

-- Exercise 12 ----------------------------------------

getCards :: Int -> Deck -> Maybe ([Card], Deck)
getCards 0 _ = Nothing
getCards n d = do
         (c, deck1)  <- nextCard d
         (cs, deck2) <- getCards (n - 1) deck1
         return (c : cs, deck2)

-- Exercise 13 ----------------------------------------

data State = State { money :: Int, deck :: Deck }

repl :: State -> IO ()
repl s@State{..} | money <= 0  = putStrLn "You ran out of money!"
                 | V.null deck = deckEmpty
                 | otherwise   = do
  putStrLn $ "You have \ESC[32m$" ++ show money ++ "\ESC[0m"
  putStrLn "Would you like to play (y/n)?"
  cont <- getLine
  if cont == "n"
  then putStrLn $ "You left the casino with \ESC[32m$"
           ++ show money ++ "\ESC[0m"
  else play
    where deckEmpty = putStrLn $ "The deck is empty. You got \ESC[32m$"
                      ++ show money ++ "\ESC[0m"
          play = do
            putStrLn "How much do you want to bet?"
            amt <- read <$> getLine
            if amt < 1 || amt > money
            then play
            else do
              case getCards 2 deck of
                Just ([c1, c2], d) -> do
                  putStrLn $ "You got:\n" ++ show c1
                  putStrLn $ "I got:\n" ++ show c2
                  case () of
                    _ | c1 >  c2  -> repl $ State (money + amt) d
                      | c1 <  c2  -> repl $ State (money - amt) d
                      | otherwise -> war s{deck = d} amt
                _ -> deckEmpty
          war (State m d) amt = do
            putStrLn "War!"
            case getCards 6 d of
              Just ([c11, c21, c12, c22, c13, c23], d') -> do
                putStrLn $ "You got\n" ++ ([c11, c12, c13] >>= show)
                putStrLn $ "I got\n" ++ ([c21, c22, c23] >>= show)
                case () of
                  _ | c13 > c23 -> repl $ State (m + amt) d'
                    | c13 < c23 -> repl $ State (m - amt) d'
                    | otherwise -> war (State m d') amt
              _ -> deckEmpty

main :: IO ()
main = evalRandIO newDeck >>= repl . State 100
