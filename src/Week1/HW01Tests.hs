-- CIS 194, Spring 2015
--
-- Test cases for HW 01

module Week1.HW01Tests where

import           Testing
import           Week1.HW01

-- Exercise 1 -----------------------------------------

testLastDigit :: (Integer, Integer) -> Bool
testLastDigit (n, d) = lastDigit n == d

testDropLastDigit :: (Integer, Integer) -> Bool
testDropLastDigit (n, d) = dropLastDigit n == d

ex1Tests :: [Test]
ex1Tests = [ Test "lastDigit test" testLastDigit
             [(123, 3), (1234, 4), (5, 5), (10, 0), (0, 0)]
           , Test "dropLastDigit test" testDropLastDigit
             [(123, 12), (1234, 123), (5, 0), (10, 1), (0,0)]
           ]

-- Exercise 2 -----------------------------------------

--testToRevDigits :: (Integer -> [Integer]) -> Bool
testToRevDigits (n, xs) = toRevDigits n == xs

ex2Tests :: [Test]
ex2Tests = [Test "toRevDigits test" testToRevDigits
            [(123, [3,2,1]), (1234, [4,3,2,1]), (-121, []), (0, [])]
            ]

-- Exercise 3 -----------------------------------------
testDoubleEveryOther :: ([Integer], [Integer]) -> Bool
testDoubleEveryOther (l1, l2) = doubleEveryOther l1 == l2

ex3Tests :: [Test]
ex3Tests = [Test "doubleEveryOher test" testDoubleEveryOther
           [([1,2,3,4], [1,4,3,8]), ([19,21,10,0], [19,42,10,0]), ([0, 0], [0, 0])]
           ]

-- Exercise 4 -----------------------------------------
testSumDigits :: ([Integer], Integer) -> Bool
testSumDigits (n, d) = sumDigits n == d

ex4Tests :: [Test]
ex4Tests = [Test "sumDigits test" testSumDigits
           [([10,5,18,4], 19), ([12,10,6,4], 14), ([0], 0), ([], 0)]
           ]

-- Exercise 5 -----------------------------------------
testLuhn :: (Integer, Bool) -> Bool
testLuhn (n, d) = luhn n == d

ex5Tests :: [Test]
ex5Tests = [Test "luhn Test" testLuhn
           [(5594589764218858, True), (1234567898765432, False)]
           ]

-- Exercise 6 -----------------------------------------
testHanoi :: (Integer, [Move]) -> Bool
testHanoi (n, moves) = hanoi n "a" "b" "c" == moves

ex6Tests :: [Test]
ex6Tests = [Test "hanoi Test" testHanoi
           [(3, [("a","b"),("a","c"),("c","a"),("a","c"),("c","a"),("c","b"),("b","c")]),
            (2, [("a","b"),("a","c"),("c","a")]),
            (1, [("a", "b")]),
            (0, [])]
           ]

-- All Tests ------------------------------------------

allTests :: [Test]
allTests = concat [ ex1Tests
                  , ex2Tests
                  , ex3Tests
                  , ex4Tests
                  , ex5Tests
                  , ex6Tests
                  ]
