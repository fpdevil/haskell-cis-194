{-# OPTIONS_GHC -Wall #-}
module HW06 where

-- import           Data.Functor
import           Data.List

-- Exercise 1 -----------------------------------------

fib :: Integer -> Integer
fib n
    | n < 0 = error "undefined for negative values"
    | n == 0 = 1
    | n == 1 = 1
    | otherwise = fib (n-1) + fib (n-2)

fibs1 :: [Integer]
fibs1 = map fib [0 .. ]

-- Exercise 2 -----------------------------------------

fibs2 :: [Integer]
fibs2 = 1 : scanl' (+) 1 fibs2
-- fibs2 = 1 : 1 : (f [1, 1]) where f tmp@(x : y : xs) = (x + y) : f (x + y : tmp)
-- fibs2 = 1 : 1 : zipWith (+) fibs2 (tail fibs2)

-- Exercise 3 -----------------------------------------

data Stream a = Cons a (Stream a)

-- Show instance prints the first 20 elements followed by ellipsis
instance Show a => Show (Stream a) where
    show s = "[" ++ intercalate ", " (map show $ take 10 $ streamToList s)
             ++ ",..."

streamToList :: Stream a -> [a]
streamToList (Cons xa xs) = xa : streamToList xs

-- Exercise 4 -----------------------------------------

instance Functor Stream where
    fmap f (Cons y ys) = Cons (f y) (fmap f ys)

-- Exercise 5 -----------------------------------------

sRepeat :: a -> Stream a
sRepeat e = Cons e (sRepeat e)

sIterate :: (a -> a) -> a -> Stream a
sIterate f x = Cons x (sIterate f (f x))

sInterleave :: Stream a -> Stream a -> Stream a
sInterleave (Cons xa1 xs1) xs2 = Cons xa1 (Cons (cfst xs2) (sInterleave xs1 (csnd xs2)))
                                 where cfst (Cons x _) = x
                                       csnd (Cons _ s) = s

sTake :: Int -> Stream a -> [a]
sTake 0 _ = []
sTake n (Cons xa xs) = xa : sTake (n-1) xs

-- Exercise 6 -----------------------------------------

nats :: Stream Integer
nats = sIterate (1+) 0

ruler :: Stream Integer
ruler = fmap (powerFunction . (+1)) nats
        where
        powerFunction v = toInteger (length (takeWhile (> 0) $ iterate (\x -> if even x then x `div` 2 else 0) v) -1)

-- take 5 $ iterate (\x -> if even x then (x `div` 2) else 0) 16--
-- [16,8,4,2,1]

-- Exercise 7 -----------------------------------------

-- | Implementation of C rand
rand :: Int -> Stream Int
rand = sIterate (\x -> (1103515245 * x + 12345) `mod` 2147483648)

-- Exercise 8 -----------------------------------------

{- Total Memory in use: ??? MB -}
minMaxSlow :: [Int] -> Maybe (Int, Int)
minMaxSlow [] = Nothing   -- no min or max if there are no elements
minMaxSlow xs = Just (minimum xs, maximum xs)

-- Exercise 9 -----------------------------------------

{- Total Memory in use: ??? MB -}
minMax :: [Int] -> Maybe (Int, Int)
minMax = loop Nothing
    where loop acc [] = acc
          loop acc (x : xs) = acc `seq` loop (case acc of
                                                Nothing -> Just (x, x)
                                                Just (l, h) | x < l -> Just (x, h)
                                                            | x > h -> Just (l, x)
                                                            | otherwise -> Just (l, h)) xs


main :: IO ()
main = print $ minMaxSlow $ sTake 1000000 $ rand 7666532

-- Exercise 10 ----------------------------------------

fastFib :: Int -> Integer
fastFib n
    | n <= 0 = 1
    | otherwise = fetchFromMat $ matrixPower n

-- λ> length . show $ fibonacci 10000001
-- 2089877
-- (0.63 secs, 152,063,376 bytes)
-- λ> map fibonacci [0..10]
-- [1,1,1,2,3,5,8,13,21,34,55]
-- (0.00 secs, 1,546,584 bytes)

-- Matrix DataType (2 X 2)
data Matrix a = Matrix a a a a deriving (Eq, Show)

-- Matrix as an Instance of a Functor
instance Functor Matrix where
    fmap f (Matrix a b c d) = Matrix (f a) (f b) (f c) (f d)

-- Matrix as an instance of Num
-- fromInteger will be a wrapper over the Identity Matrix
instance Num a => Num (Matrix a) where
    (Matrix x1 x2 x3 x4) + (Matrix y1 y2 y3 y4) = Matrix (x1 + y1) (x2 + y2) (x3 + y3) (x4 + y4)
    (Matrix x1 x2 x3 x4) - (Matrix y1 y2 y3 y4) = Matrix (x1 - y1) (x2 - y2) (x3 - y3) (x4 - y4)
    (Matrix x1 x2 x3 x4) * (Matrix y1 y2 y3 y4) =
        Matrix (x1*y1 + x2*y3) (x1*y2 + x2*y4) (x3*y1 + x4*y3) (x3*y2 + x4*y4)
    fromInteger a = Matrix (fromInteger a) 0 0 (fromInteger a)
    negate = fmap negate
    abs = fmap abs
    signum = fmap signum

-- Get Matrix to the power of a value
powerOfMatrix :: (Num a, Integral b) => Matrix a -> b -> Matrix a
powerOfMatrix mat n = mat ^ n

-- Default starting Matrix is Matrix 1 1 1 0
matrixPower :: (Num a) => Int -> Matrix a
matrixPower = powerOfMatrix (Matrix 1 1 1 0)

fetchFromMat :: (Num a) => Matrix a -> a
fetchFromMat (Matrix _ x _ _) = x

-- Test functions
fibonacci :: Int -> Integer
fibonacci n
    | n <= 0 = 1
    | otherwise = fetchFromMat $ matrixPower n

fibSeries :: Int -> [Integer]
fibSeries n = fmap fibonacci [0 .. n]

