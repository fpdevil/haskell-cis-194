-- CIS 194, Spring 2015
--
-- Test cases for HW 04

module Week4.HW04Tests where

import           Testing
import           Week4.HW04

-- P [1,2,3] == P [1,2,3,0]
ex2Tests :: [Test]
ex2Tests = [testF2 "show ==" (==)
             [
               (P [1,2,3], P [1,2,3,0], True)
             ]
           ]

ex3Tests :: [Test]
ex3Tests = [testF1 "show test" show
             [
               (P [0], "0")
               ,(P [1], "1")
               ,(P [1,2,3], "3x^2 + 2x + 1")
               ,(P [-3,0,2], "2x^2 + -3")
               ,(P [1,0,0,2], "2x^3 + 1")
               ,(P [0,-1,2], "2x^2 + -x")
             ]
           ]

ex4Tests :: [Test]
ex4Tests = [testF2 "plus test" plus
             [
               (P [5, 0, 1], P [1, 1, 2], P [6, 1, 3])
               ,(P [1, 0, 1], P [1, 1], P [2, 1, 1])
             ]
           ]

ex5Tests :: [Test]
ex5Tests = [testF2 "times test" times
             [
               (P [1, 1, 1], P [2, 2], P [2, 4, 4, 2])
             ]
           ]

ex6Tests :: [Test]
ex6Tests = [testF1 "negate test" negate
              [(P [1, 2, 3], P [-1, -2, -3])]
             ,testF2 "fromInteger test" plus
              [(P [2,1], 8, P[10,1])]
           ]

ex7Tests :: [Test]
ex7Tests = [testF2 "applyP test" applyP
             [
               (P[2,0,3], 2, 14)
               ,(P[2,3], 2, 8)
               ,(P[1,2,1], 1, 4)
               ,(P[1,2,1], 2, 9)
             ]
           ]

--deriv (x^2 + 3*x + 5) == 2*x + 3
ex9Tests :: [Test]
ex9Tests = [testF1 "deriv test" deriv
              [(P [5,3,1], P [3,2])]
           ]


-- All Tests ------------------------------------------

allTests :: [Test]
allTests = concat [
                    ex2Tests
                    ,ex3Tests
                    ,ex4Tests
                    ,ex5Tests
                    ,ex6Tests
                    ,ex7Tests
                    ,ex9Tests
                  ]

