{-# OPTIONS_GHC -Wall #-}
module Week2.HW02 where

-- Mastermind -----------------------------------------

-- A peg can be one of six colors
data Peg = Red | Green | Blue | Yellow | Orange | Purple
         deriving (Show, Eq, Ord)

-- A code is defined to simply be a list of Pegs
type Code = [Peg]

-- A move is constructed using a Code and two integers; the number of
-- exact matches and the number of regular matches
data Move = Move Code Int Int
          deriving (Show, Eq)

-- List containing all of the different Pegs
colors :: [Peg]
colors = [Red, Green, Blue, Yellow, Orange, Purple]

-- Exercise 1 -----------------------------------------

-- Get the number of exact matches between the actual code and the guess
exactMatches :: Code -> Code -> Int
exactMatches [] _ = 0
exactMatches _ [] = 0
exactMatches (x : xs) (y : ys)
    | x == y = 1 + exactMatches xs ys
    | otherwise = exactMatches xs ys

-- Exercise 2 -----------------------------------------

-- For each peg in xs, count how many times is occurs in ys
countColors :: Code -> [Int]
countColors c = [foldr (\y acc -> if y == x then acc + 1 else acc) 0 c | x <- colors]

-- Count number of matches between the actual code and the guess
matches :: Code -> Code -> Int
matches codeA codeB = foldr (\(a, b) acc -> acc + (a `min` b)) 0 (zip (countColors codeA) (countColors codeB))

-- Exercise 3 -----------------------------------------

-- Construct a Move from a guess given the actual code
getMove :: Code -> Code -> Move
getMove secret guess = Move guess x y
    where
        x = exactMatches secret guess
        y = matches secret guess - x

-- Exercise 4 -----------------------------------------

isConsistent :: Move -> Code -> Bool
isConsistent (Move guess a b) secret = getMove secret guess == Move guess a b

-- Exercise 5 -----------------------------------------

filterCodes :: Move -> [Code] -> [Code]
filterCodes guessMove = filter (isConsistent guessMove)

-- Exercise 6 -----------------------------------------

allCodes :: Int -> [Code]
allCodes 0 = [[]]
allCodes 1 = [[c] | c <- colors]
allCodes n = concat $ foldr (\x acc -> [x : y | y <- allCodes (n - 1)] : acc) [[]] colors

-- Exercise 7 -----------------------------------------

solve :: Code -> [Move]
solve secret = solverAux (allCodes $ length secret)
    where
        solverAux :: [Code] -> [Move]
        solverAux [] = []
        solverAux (x : xs)
          | x /= secret = getMove secret x : solverAux (filterCodes (getMove secret x) xs)
          | otherwise = getMove secret x : solverAux xs

-- Bonus ----------------------------------------------

fiveGuess :: Code -> [Move]
fiveGuess = undefined
