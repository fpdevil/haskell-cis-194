module Main where

import           Testing
import           Week1.HW01       as W1
import           Week1.HW01Tests  as W1T
import           Week2.HW02       as W2
import           Week2.HW02Tests  as W2T
import           Week3.HW03       as W3
import           Week3.HW04.Tests as W4T
import           Week4.HW04       as W4

main :: IO ()
main = do
  putStrLn "Run the tests"
  print $ runTests W1T.allTests
  print $ runTests W2T.allTests
  print $ runTests W4T.allTests
