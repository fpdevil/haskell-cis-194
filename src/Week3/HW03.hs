module Week3.HW03 where

data Expression =
    Var String                   -- Variable
  | Val Int                      -- Integer literal
  | Op Expression Bop Expression -- Operation
  deriving (Show, Eq)

-- Binary (2-input) operators
data Bop =
    Plus
  | Minus
  | Times
  | Divide
  | Gt
  | Ge
  | Lt
  | Le
  | Eql
  deriving (Show, Eq)

data Statement =
    Assign   String     Expression
  | Incr     String
  | If       Expression Statement  Statement
  | While    Expression Statement
  | For      Statement  Expression Statement Statement
  | Sequence Statement  Statement
  | Skip
  deriving (Show, Eq)

type State = String -> Int

-- Exercise 1 -----------------------------------------

extend :: State -> String -> Int -> State
extend existingState var val = nextState
    where nextState :: State
          nextState newvar
            | newvar == var = val
            | otherwise = existingState newvar

{--
λ> let st' = extend empty "A" 5 in st' "A" == 5
        True
λ> let st' = extend empty "A" 5 in st' "A" == 6
        False
λ> (extend empty "A" 5) "A"
        5
--}

empty :: State
empty _ = 0

-- Exercise 2 -----------------------------------------

evalE :: State -> Expression -> Int
evalE state expr = case expr of
                         Var var -> state var
                         Val val -> val
                         Op expr1 binop expr2 -> evalO binop (evalE state expr1) (evalE state expr2)

-- Auxilliary function for evaluating the Operator
--
evalO :: Bop -> Int -> Int -> Int
evalO bop e1 e2 = case bop of
                       Plus -> e1 + e2
                       Minus -> e1 - e2
                       Times -> e1 * e2
                       Divide -> e1 `div` e2
                       Gt -> if e1 > e2 then 1 else 0
                       Lt -> if e1 < e2 then 1 else 0
                       Ge -> if e1 >= e2 then 1 else 0
                       Le -> if e1 <= e2 then 1 else 0
                       Eql -> if e1 == e2 then 1 else 0


-- Exercise 3 -----------------------------------------

data DietStatement = DAssign String Expression
                   | DIf Expression DietStatement DietStatement
                   | DWhile Expression DietStatement
                   | DSequence DietStatement DietStatement
                   | DSkip
                     deriving (Show, Eq)

desugar :: Statement -> DietStatement
desugar stmt = case stmt of
                    Assign str e -> DAssign str e
                    Incr str -> DAssign str (Op (Var str) Plus (Val 1))
                    If e stmt1 stmt2 -> DIf e (desugar stmt1) (desugar stmt2)
                    While e statement -> DWhile e (desugar statement)
                    Sequence stmt1 stmt2 -> DSequence (desugar stmt1) (desugar stmt2)
                    Skip -> DSkip
                    -- TODO
                    -- For stmt1 e stmt2 stmt3 -> undefined


-- Exercise 4 -----------------------------------------

evalSimple :: State -> DietStatement -> State
evalSimple state dstmt = case dstmt of
                              DAssign str e -> extend state str (evalE state e)
                              DIf e dstmt1 dstmt2 -> if (evalE state e) /= 0
                                                       then evalSimple state dstmt1
                                                       else evalSimple state dstmt2
                              DWhile e dstmt -> if (evalE state e) /= 0
                                                   then evalSimple (evalSimple state dstmt) (DWhile e dstmt)
                                                   else state
                              DSkip -> state
                              -- TODO
                              -- DSequence ->

run :: State -> Statement -> State
run = undefined

-- Programs -------------------------------------------

slist :: [Statement] -> Statement
slist [] = Skip
slist l  = foldr1 Sequence l

{- Calculate the factorial of the input

   for (Out := 1; In > 0; In := In - 1) {
     Out := In * Out
   }
-}
factorial :: Statement
factorial = For (Assign "Out" (Val 1))
                (Op (Var "In") Gt (Val 0))
                (Assign "In" (Op (Var "In") Minus (Val 1)))
                (Assign "Out" (Op (Var "In") Times (Var "Out")))


{- Calculate the floor of the square root of the input

   B := 0;
   while (A >= B * B) {
     B++
   };
   B := B - 1
-}
squareRoot :: Statement
squareRoot = slist [ Assign "B" (Val 0)
                   , While (Op (Var "A") Ge (Op (Var "B") Times (Var "B")))
                       (Incr "B")
                   , Assign "B" (Op (Var "B") Minus (Val 1))
                   ]

{- Calculate the nth Fibonacci number

   F0 := 1;
   F1 := 1;
   if (In == 0) {
     Out := F0
   } else {
     if (In == 1) {
       Out := F1
     } else {
       for (C := 2; C <= In; C++) {
         T  := F0 + F1;
         F0 := F1;
         F1 := T;
         Out := T
       }
     }
   }
-}
fibonacci :: Statement
fibonacci = slist [ Assign "F0" (Val 1)
                  , Assign "F1" (Val 1)
                  , If (Op (Var "In") Eql (Val 0))
                       (Assign "Out" (Var "F0"))
                       (If (Op (Var "In") Eql (Val 1))
                           (Assign "Out" (Var "F1"))
                           (For (Assign "C" (Val 2))
                                (Op (Var "C") Le (Var "In"))
                                (Incr "C")
                                (slist
                                 [ Assign "T" (Op (Var "F0") Plus (Var "F1"))
                                 , Assign "F0" (Var "F1")
                                 , Assign "F1" (Var "T")
                                 , Assign "Out" (Var "T")
                                 ])
                           )
                       )
                  ]
