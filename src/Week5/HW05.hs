{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Week5.HW05 where

import           Control.Applicative
import           Data.Bits            (xor)
import           Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as BS
import           Data.List            (sortBy)
import           Data.Map.Strict      (Map)
import qualified Data.Map.Strict      as Map
import           Data.Maybe           (fromMaybe)
import qualified Data.Ord             as O
import           Parser
import           System.Environment   (getArgs)

-- Exercise 1 -----------------------------------------

getSecret :: FilePath -> FilePath -> IO ByteString
getSecret encodedFile originalFile = do
    encodedBitString <- BS.readFile encodedFile
    originalBitString <- BS.readFile originalFile
    let pairwiseXOR = BS.zipWith xor encodedBitString originalBitString
    return $ BS.pack $ filter (/= 0) pairwiseXOR

-- λ> getSecret "dog.jpg" "dog-original.jpg"
-- "Haskell Is Great!"

-- Exercise 2 -----------------------------------------

decryptWithKey :: ByteString -> FilePath -> IO ()
decryptWithKey secretKey filePath = do
    encryptedFile <- BS.readFile (filePath ++ ".enc")
    let decipheredXOR = BS.zipWith xor encryptedFile (BS.cycle secretKey)
     in BS.writeFile filePath $ BS.pack decipheredXOR

-- secret <- getSecret "dog.jpg" "dog-original.jpg"
-- decryptWithKey secret "victims.json"
-- Exercise 3 -----------------------------------------

parseFile :: FromJSON a => FilePath -> IO (Maybe a)
parseFile victimsFile = decode <$> BS.readFile victimsFile

-- λ> parseFile "victims.json"
-- Nothing
-- λ> parseFile "victims.json" :: IO (Maybe [TId])

-- Exercise 4 -----------------------------------------

getBadTs :: FilePath -> FilePath -> IO (Maybe [Transaction])
getBadTs victimData transactionData = do
    victimInfo <- parseFile victimData :: IO (Maybe [TId])
    transactionInfo <- parseFile transactionData :: IO (Maybe [Transaction])
    let trData = fromMaybe [] transactionInfo
        vcData = fromMaybe [] victimInfo
        victimTransactions = (\a b -> if tid a == b then Just a else Nothing) <$> trData <*> vcData
        filteredResults = filter (/= Nothing) victimTransactions
    return $ sequence filteredResults

-- λ> length (fromMaybe [] ll)
-- 182

-- Exercise 5 -----------------------------------------

getFlow :: [Transaction] -> Map String Integer
getFlow [] = Map.empty
getFlow transactionList = foldr func Map.empty transactionList
    where
    func a b = Map.insertWith (-) (from a) (amount a) $ Map.insertWith (+) (to a) (amount a) b

-- Exercise 6 -----------------------------------------

getCriminal :: Map String Integer -> String
getCriminal transactionMap
    | tupleList /= [] = (fst . head) tupleList
    | otherwise = ""
      where
          tupleList = sortBy (\(_, v1) (_, v2) -> v2 `compare` v1) $ Map.toList transactionMap

{--
-- λ> z <- getBadTs "victims.json" "transactions.json"
-- λ> getCriminal $ getFlow $ head $ Data.Maybe.maybeToList z
-- "Shaanan Cohney"
-- λ> getCriminal <$> fmap getFlow z
-- Just "Shaanan Cohney"
-- λ> getCriminal <$> getFlow <$> Data.Maybe.fromJust <$> (getBadTs "victims.json" "transactions.json")
-- "Shaanan Cohney"
--}

-- Exercise 7 -----------------------------------------
-- As per the instructions
-- separate the people into payers and payees ie, people who
-- ended up with extra money and people who ended up at a loss.
-- payers ended up with extra, payees ended up with loss
undoTs :: Map String Integer -> [TId] -> [Transaction]
undoTs flowMap trIDList = getZipList $ trFunc <$> payerZ <*> payeeZ <*> tridZ
    where
        payers = Map.toList $ Map.filter (> 0) flowMap
        payees = Map.toList $ Map.filter (<= 0) flowMap
        payerZ = ZipList $ sortBy ((O.comparing. (O.Down .)) snd) payers
        payeeZ = ZipList $ sortBy (O.comparing snd) payees
        tridZ = ZipList trIDList
        trFunc x y t = Transaction { from = fst x
                                   , to = fst y
                                   , amount = snd x - snd y
                                   , tid = t
                                   }

-- Exercise 8 -----------------------------------------

writeJSON :: ToJSON a => FilePath -> a -> IO ()
writeJSON fp datax = BS.writeFile fp (encode datax)

-- Exercise 9 -----------------------------------------

doEverything :: FilePath -> FilePath -> FilePath -> FilePath -> FilePath
             -> FilePath -> IO String
doEverything dog1 dog2 trans vict fids out = do
  key <- getSecret dog1 dog2
  decryptWithKey key vict
  mts <- getBadTs vict trans
  case mts of
    Nothing -> error "No Transactions"
    Just ts -> do
      mids <- parseFile fids
      case mids of
        Nothing  -> error "No ids"
        Just ids -> do
          let flow = getFlow ts
          writeJSON out (undoTs flow ids)
          return (getCriminal flow)

main :: IO ()
main = do
  args <- getArgs
  crim <-
    case args of
      dog1:dog2:trans:vict:ids:out:_ ->
          doEverything dog1 dog2 trans vict ids out
      _ -> doEverything "dog-original.jpg"
                        "dog.jpg"
                        "transactions.json"
                        "victims.json"
                        "new-ids.json"
                        "new-transactions.json"
  putStrLn crim

