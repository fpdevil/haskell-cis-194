# (Haskell CIS194 Course Assignments)

Code for the exercises accompanying the [CIS194](http://www.seas.upenn.edu/~cis194/lectures.html)

Very much a work in progress :running:

Finished till now.
 * Week1 :heavy_check_mark:
 * Week2 :heavy_check_mark:
 * Week3 :heavy_check_mark:
 * Week4 :heavy_check_mark:
 * Week5 :heavy_check_mark:
 * Week6 :heavy_check_mark:
 * Week7 :heavy_check_mark:

Yet to finish

 * Week8  :x:
 * Week9  :x:
 * Week10 :x:
 * Week11 :x:
 * Week12 :x:

And here's the way to run test cases from ghci :exclamation:

If all cases are good we get null response :+1:

```haskell
main
```
Else we get an error :-1:

### Links referred:
 * [The Excellent LYAH](http://learnyouahaskell.com) This is the book which got me into Haskell
 * [ZVON Haskell Reeference](http://zvon.org/comp/r/ref-Haskell.html) for some excellent function usage illustrations
 * [Haskell Code](https://hackage.haskell.org/) Got most of the ideas of Type Classes from here
 * [Diagrams](http://projects.haskell.org/diagrams/doc/quickstart.html) Haskell Diagrams Package
 * [Category Theory](http://yogsototh.github.io/Category-Theory-Presentation/) Category Theory Fundamentals
