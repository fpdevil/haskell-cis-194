{-# OPTIONS_GHC -Wall #-}
module Week4.HW04 where

import           Data.List (groupBy)

newtype Poly a = P [a]

-- Exercise 1 -----------------------------------------

x :: Num a => Poly a
x = P [0, 1]

-- Exercise 2 ----------------------------------------

instance (Num a, Eq a) => Eq (Poly a) where
    (==) (P y) (P z) = trimz (reverse y) == trimz (reverse z)

-- auxilliary function for removing zeros at front
trimz :: (Num a, Eq a) => [a] -> [a]
trimz (0 : cs) = trimz cs
trimz cs = cs

-- Exercise 3 -----------------------------------------

instance (Num a, Eq a, Show a) => Show (Poly a) where
    show (P a) = displayPoly a

-- auxilliary function for displaying polynomial
displayPoly :: (Eq a, Num a, Show a) => [a] -> String
displayPoly [] = show (0 :: Integer)
displayPoly [0] = show (0 :: Integer)
displayPoly poly = let coefficients = filter (\(c, _) -> c /= 0) $ zip poly [0 ..]
                       fun (a, b)
                        | a == 1 && b == 0 = "1"
                        | a == (-1) && b == 0 = "-1"
                        | a == (-1) && b /= 0 = '-' : showVar b
                        | a == 0 || a == 1 = "" ++ showVar b
                        | otherwise = show a ++ showVar b
                       mappings = map fun coefficients
                    in foldr1 (\y acc -> acc ++ " + " ++ y) mappings

-- auxilliary function for displaying the degree or powers of the Polynomial
showVar :: (Eq a, Num a, Show a) => a -> String
showVar degree = case degree of
                   0 -> ""
                   1 -> "x"
                   d -> "x" ++ "^" ++ show d

-- Exercise 4 -----------------------------------------

plus :: Num a => Poly a -> Poly a -> Poly a
plus (P px) (P py)
    | length px == length py = P (zipPoly px py)
    | length px > length py = P (zipPoly px zpy)
    | otherwise = P (zipPoly zpx py)
      where
          zipPoly = zipWith (+)
          zpx = px ++ replicate (length py - length px) 0
          zpy = py ++ replicate (length px - length py) 0

-- Exercise 5 -----------------------------------------

times :: Num a => Poly a -> Poly a -> Poly a
times (P []) _ = P []
times _ (P []) = P []
times (P pa) (P pb) = P mulPoly
    where
        pax = zip pa [0 .. ]
        pby = zip pb [0 .. ]
        polyList = if length pa >= length pb
                   then concat [[(b+d,a*c) | (c,d) <- pby] | (a,b) <- pax]
                   else concat [[(b+d,a*c) | (c,d) <- pax] | (a,b) <- pby]
        mappedList = map (map snd) $ groupBy (\(s1,_) (s2,_) -> s1 == s2) polyList
        mulPoly = map (foldr (+) 0) mappedList

-- Exercise 6 -----------------------------------------

instance Num a => Num (Poly a) where
    (+) = plus
    (*) = times
    negate (P h) = P (map negate h)
    fromInteger f = P [fromIntegral f]
    -- No meaningful definitions exist
    abs    = didNotImplement "abs"
    signum = didNotImplement "signum"

-- auxilliary function for unimplemented method declaration
didNotImplement :: String -> a
didNotImplement str = error $ "Polynomial method " ++ str ++ " cannot be implemented"

-- Exercise 7 -----------------------------------------

applyP :: Num a => Poly a -> a -> a
applyP (P []) _ = 0
applyP (P [n]) _ = n
applyP (P plx) n = sum appliedVals
    where
        appliedVals = zipWith (*) plx $ map (n^) [0 .. ]

-- applyP (x^2 + 2*x + 1) 1 == 4
-- applyP (x^2 + 2*x + 1) 2 == 9

-- Exercise 8 -----------------------------------------

class Num a => Differentiable a where
    deriv  :: a -> a
    nderiv :: Int -> a -> a
    nderiv n fun
        | n < 0 = error "Negative order derivative is not possible"
        | n == 0 = fun
        | n == 1 = deriv fun
        | otherwise = deriv $ nderiv (n-1) fun

-- Exercise 9 -----------------------------------------

instance Num a => Differentiable (Poly a) where
    deriv (P []) = error "Improper Polynomial"
    deriv (P [_]) = P [0]
    deriv (P polyx) = P derivx
        where
            zippedVals = zip polyx (iterate (+1) 0)
            derivVals = [(a*b,b-1) | (a,b) <- tail zippedVals]
            derivx = (fst . unzip) derivVals

--
--λ> deriv (4*x^3 + 2*x + 1)
--12x^2 + 2
--λ> nderiv 1 (4*x^3 + 2*x + 1)
--12x^2 + 2
--λ> nderiv 2 (4*x^3 + 2*x + 1)
--24x
--λ> nderiv 3 (4*x^3 + 2*x + 1)
--24
